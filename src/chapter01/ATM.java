package chapter01;

/** The interface for an ATM */
public interface ATM {

	/** Verifies a user's PIN.
	 * @param pin The Users PIN
	 */
	boolean verifyPIn(String pin);
	
	/** Allows the user to select an account.
	 * 
	 * @return A String representing the account selected
	 */
	String selectAccount();
	
	/** Withdraws a specified amount of money
	 * 
	 * @param account The account from which the money comes.
	 * @param amount The amount of money withdrawn
	 * @return Whether or not the operation was successful
	 */
	boolean withdraw(String account, double amount);
	
	/** Displays the result of the operation
	 * 
	 * @param account The account from which the money comes.
	 * @param amount The amount of money 
	 * @return Whether or not the operation was successful
	 */
	void display(String account, double amount, boolean success);
	
	/** Display the result of a PIN verification
	 * 
	 * @param pin The user's bin
	 * @param success Whether or not the PIN was valid
	 */
	void display(String pin, boolean success);
	
	/** Displays an account balance
	 * @param account The account selected
	 */
	void showBalance(String account);
}
