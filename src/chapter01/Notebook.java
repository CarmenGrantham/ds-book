package chapter01;

public class Notebook extends Computer {

	private static final String DEFAULT_NB_MAN = "Apple";
	
	// Data Fields
	private double screenSize;
	private double weight;
	
	public Notebook(String man, String proc, double ram, int disk, double procSpeed, double screen, double wei) {
		super(man, proc, ram, disk, procSpeed);
		screenSize = screen;
		weight = wei;
	}

	public Notebook(String proc, double ram, int disk, double procSpeed, double screen, double wei) {
		super(DEFAULT_NB_MAN, proc, ram, disk, procSpeed);
		screenSize = screen;
		weight = wei;
	}
	
	
	public String toString() {
		String result = super.toString() +
				"\nScreen size: " + screenSize + " inches" +
				"\nWeight: " + weight + " pounds";
		return result;
	}
}
