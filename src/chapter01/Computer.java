package chapter01;

public class Computer {

	// Data fields
	private String manufacturer;
	private String processor;
	private double ramSize;
	private int diskSize;
	private double processorSpeed;
	
	// Methods
	public Computer(String man, String processor, double ram, int disk, double procSpeed) {
		manufacturer = man;
		this.processor = processor;
		ramSize = ram;
		diskSize = disk;
		processorSpeed = procSpeed;
	}
	
	public double computePower() { return ramSize * processorSpeed; }
	public double getRamSize() { return ramSize; }
	public double getProcessorSpeed() { return processorSpeed;	}
	public int getDiskSize() { return diskSize; }
	public String getManufacturer() { return manufacturer; }
	
	public int comparePower(Computer aComputer) {
		if (this.computePower() < aComputer.computePower()) {
			return -1;
		} else if (this.computePower() == aComputer.computePower()) {
			return 0;
		}
		return 1;
	}
	
	public String toString() {
		String result = "Manufacturer: " + manufacturer +
				"\nCPU: " + processor +
				"\nRAM: " + ramSize + " megabytes" +  
				"\nDisk: " + diskSize + " gigabytes" +
				"\nProcessor speed: " + processorSpeed + " gigahertz";
		return result;
	}
	
}
