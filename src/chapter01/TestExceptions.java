package chapter01;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TestExceptions {

	public static int getIntValue(Scanner scan) {
		int nextInt = 0;			// next int value
		boolean validInt = false;	// Flag for valid input
		while (!validInt) {
			try {
				System.out.println("Enter number of kids:");
				nextInt = scan.nextInt();
				validInt = true;
			} catch (InputMismatchException ex) {
				scan.nextLine();			// clear buffer
				System.out.println("Bad data -- enter an integer:");
			}
		}
		return nextInt;
	}	
	
	public static void processPositiveInteger(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Invalid negative argument");
		} else {
			// Process n as required
			// ...
			System.out.println("Finished processing " + n);
		}
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			int num = getIntValue(scan);
			processPositiveInteger(num);
		} catch (IllegalArgumentException ex) {
			System.err.println(ex.getMessage());
			System.exit(1);			// error indication
		}
		System.exit(0);				// normal exit
	}
}
