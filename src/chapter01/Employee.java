package chapter01;

public class Employee {

	// Data Fields
	private String name;
	private double hours;
	private double rate;
	
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj == null) return false;
		if (this.getClass() == obj.getClass()) {
			Employee other = (Employee) obj;
			return name.equals(other.name);
		}
		return false;
	}
}
