package chapter01;

public abstract class Food {
	// Data Field
	private double calories;
	
	// Abstract Methods
	public abstract double percentProtein();
	public abstract double percentFat();
	public abstract double percentCarbohydrates();
	
	// Actual Methods
	public double getCalories() { return calories; }
	public void setCalories(double cal) {
		calories = cal;
	}
}
