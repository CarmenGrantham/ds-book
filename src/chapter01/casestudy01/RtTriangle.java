package chapter01.casestudy01;

import java.util.Scanner;

public class RtTriangle extends Shape {

	// Data Fields
	public double base = 0;
	private double height = 0;
	
	public RtTriangle() {
		super("RtTriangle");
	}
	
	public RtTriangle(double base, double height) {
		this();
		this.base = base;
		this.height = height;
	}
	
	public double getBase() {
		return base;
	}
	
	public double getHeight() {
		return height;
	}
	
	@Override
	public double computeArea() {
		return (base * base * height) / 2;
	}
	
	@Override
	public double computePerimeter() {
		return base + base + height;
	}
	
	@Override
	public void readShapeData() {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the base of the Rectangle: ");
		base = in.nextDouble();
		System.out.println("Enter the height of the Rectangle: ");
		height = in.nextDouble();
	}
	
	@Override
	public String toString() {
		return super.toString() + ": base is " + base +
				", height is " + height;
	}
}
