package chapter01.casestudy01;

import java.util.Scanner;

public class Circle extends Shape {

	// Data Fields
	public double radius = 0;
	
	public Circle() {
		super("Circle");
	}
	
	public Circle(double radius) {
		this();
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	@Override
	public double computeArea() {
		return radius * radius * Math.PI;
	}
	
	@Override
	public double computePerimeter() {
		return Math.PI * 2 * radius;
	}
	
	@Override
	public void readShapeData() {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the radius of the Rectangle: ");
		radius = in.nextDouble();
	}
	
	@Override
	public String toString() {
		return super.toString() + ": radius is " + radius;
	}
}
