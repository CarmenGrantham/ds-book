package chapter01;

public class TestNotebook {

	public static void main(String[] args) {
		Computer myComputer = new Computer("Acme", "Intel", 2, 160, 2.4);
		Notebook yourComputer = new Notebook("DellGate", "AMD", 4, 240, 1.8, 15.0, 7.5);
		Notebook johnsComputer = new Notebook("Intel", 5, 260, 1.8, 15.0, 13.5);
		System.out.println("My computer is:\n" + myComputer.toString());
		System.out.println("\nYour Computer is:\n" + yourComputer.toString());
		System.out.println("\nJohn's Computer is:\n" + johnsComputer.toString());
		
		
		int compare1 = myComputer.comparePower(yourComputer);
		int compare2 = myComputer.comparePower(myComputer);
		int compare3 = johnsComputer.comparePower(yourComputer);
		
		System.out.println("Comparison 1: " + compare1 + "\nComparison 2: " + compare2 + "\nComparison 3: " + compare3);
	}
}
