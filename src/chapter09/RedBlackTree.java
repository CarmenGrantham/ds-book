package chapter09;

/** Class to represent Red-Black tree. */
public class RedBlackTree<E extends Comparable<E>> extends
        BinarySearchTreeWithRotate<E> {

    private static class RedBlackNode<E> extends Node<E> {
        /** Colour indicator. True if red, false if black. */
        private boolean isRed;

        /**
         * Create a RedBlackNode with the default colour of red and the given
         * data field.
         * 
         * @param item
         *            The data field.
         */
        public RedBlackNode(E item) {
            super(item);
            isRed = true;
        }

        public String toString() {
            if (isRed) {
                return "Red: " + super.toString();
            } else {
                return "Black: " + super.toString();
            }
        }
    }

    public boolean add(E item) {
        if (root == null) {
            root = new RedBlackNode<E>(item);
            ((RedBlackNode<E>) root).isRed = false; // root is black;
            return true;
        } else {
            root = add((RedBlackNode<E>) root, item);
            ((RedBlackNode<E>) root).isRed = false; // root is always black.
            return addReturn;
        }
    }

    private Node<E> add(RedBlackNode<E> localRoot, E item) {
        if (item.compareTo(localRoot.data) == 0) {
            // item already in the tree.
            addReturn = false;
            return localRoot;
        } else if (item.compareTo(localRoot.data) < 0) {
            // item < localRoot.data
            if (localRoot.left == null) {
                // Create new left child.
                localRoot.left = new RedBlackNode<E>(item);
                addReturn = true;
                return localRoot;
            } else { // Need to search
                // Check for 2 red children, swap colours if found
                moveBlackDown(localRoot);
                // recursively add on the left.
                localRoot.left = add((RedBlackNode<E>) localRoot.left, item);

                // See whether the left child is now red
                if (((RedBlackNode<E>) localRoot.left).isRed) {
                    if (localRoot.left.left != null
                            && ((RedBlackNode<E>) localRoot.left.left).isRed) {
                        // Left-left grandchild is also red

                        // Single rotation is necessary
                        ((RedBlackNode<E>) localRoot.left).isRed = false;
                        localRoot.isRed = true;
                        return rotateRight(localRoot);
                    } else if (localRoot.left.right != null
                            && ((RedBlackNode<E>) localRoot.left.right).isRed) {
                        // Left-right grandchild is also red

                        // Double rotation is necessary
                        localRoot.left = rotateLeft(localRoot.left);
                        ((RedBlackNode<E>) localRoot.left).isRed = false;
                        localRoot.isRed = true;
                        return rotateRight(localRoot);
                    }
                }
            }
            return localRoot;
        } else {
            // item > localRoot.data
            if (localRoot.right == null) {
                // Create new right child.
                localRoot.right = new RedBlackNode<E>(item);
                addReturn = true;
                return localRoot;
            } else { // Need to search
                // Check for 2 red children, swap colours if found
                moveBlackDown(localRoot);
                // recursively add on the right.
                localRoot.right = add((RedBlackNode<E>) localRoot.right, item);

                // See whether the right child is now red
                if (((RedBlackNode<E>) localRoot.right).isRed) {
                    if (localRoot.right.right != null
                            && ((RedBlackNode<E>) localRoot.right.right).isRed) {
                        // Left-left grandchild is also red

                        // Single rotation is necessary
                        ((RedBlackNode<E>) localRoot.right).isRed = false;
                        localRoot.isRed = true;
                        return rotateRight(localRoot);
                    } else if (localRoot.right.left != null
                            && ((RedBlackNode<E>) localRoot.right.left).isRed) {
                        // Left-right grandchild is also red

                        // Double rotation is necessary
                        localRoot.right = rotateRight(localRoot.right);
                        ((RedBlackNode<E>) localRoot.right).isRed = false;
                        localRoot.isRed = true;
                        return rotateLeft(localRoot);
                    }
                }
            }
            return localRoot;
        }
    }

    /**
     * Method to make the two children of the a sub-tree black and the localRoot
     * black.
     * 
     * @param localRoot
     *            The root of the sub-tree
     */
    private void moveBlackDown(RedBlackNode<E> localRoot) {
        // see if both children are red
        if (localRoot.left != null && localRoot.right != null
                && ((RedBlackNode<E>) localRoot.left).isRed
                && ((RedBlackNode<E>) localRoot.right).isRed) {
            // make them black and myself red
            ((RedBlackNode<E>) localRoot.left).isRed = false;
            ((RedBlackNode<E>) localRoot.right).isRed = false;
            localRoot.isRed = true;
        }
    }
}
