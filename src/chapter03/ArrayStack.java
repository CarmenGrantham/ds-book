package chapter03;

import java.util.Arrays;
import java.util.EmptyStackException;

public class ArrayStack<E> implements StackInt<E>{

    private E[] theData;
    int topOfStack = -1;        // Initially empty stack
    private static final int INITIAL_CAPACITY = 10;
    
    public ArrayStack() {
        theData = (E[])new Object[INITIAL_CAPACITY];
    }
    
    @Override
    public E push(E obj) {
        if (topOfStack == theData.length - 1) {
            reallocate();
        }
        topOfStack++;
        theData[topOfStack] = obj;
        return obj;
    }
    
    @Override
    public E pop() {
        if (empty()) {
            throw new EmptyStackException();
        }
        return theData[topOfStack--];
    }
    

    private void reallocate() {
        int capacity = 2 * theData.length;
        theData = Arrays.copyOf(theData, capacity);
    }

    @Override
    public E peek() {
        if (empty()) {
            throw new EmptyStackException();
        }
        return theData[topOfStack];
    }

    @Override
    public boolean empty() {
        return topOfStack == -1;
    }
    
}
