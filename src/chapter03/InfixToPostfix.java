package chapter03;

import java.util.EmptyStackException;
import java.util.Stack;

public class InfixToPostfix {
    public static class SyntaxErrorException extends Exception {
        public SyntaxErrorException(String message) {
            super(message);
        }
    }
    
    private Stack<Character> operatorStack;
    private static final String OPERATORS="+-*/";
    private static final int[] PRECEDENCE = {1, 1, 2, 2};
    private StringBuilder postfix;
    
    public String convert(String infix) throws SyntaxErrorException {
        operatorStack = new Stack<Character>();
        postfix = new StringBuilder();
        
        String[] tokens = infix.split("\\s+");
        try {
            // Process each token in the infix string.
            for (String nextToken : tokens) {
                char firstChar = nextToken.charAt(0);
                // Is it an operand?
                if (Character.isJavaIdentifierStart(firstChar) || Character.isDigit(firstChar)) {
                    postfix.append(nextToken);
                    postfix.append(" ");
                } else if(isOperator(firstChar)) {     // Is it an operator?
                    processOperator(firstChar);
                } else {
                    throw new SyntaxErrorException("Unexpected Character Encountered: " + firstChar);
                }
            }
            
            // Pop any remaining operators and append them to postfix
            while (!operatorStack.empty()) {
                char op = operatorStack.pop();
                postfix.append(op);
                postfix.append(" ");
            }
            // assert: Stack is empty, return result
            return postfix.toString();
        } catch (EmptyStackException ex) {
            throw new SyntaxErrorException("Syntax Error: The stack is empty");
        }
    }
    
    private void processOperator(char op) {
        if (operatorStack.empty()) {
            operatorStack.push(op);
        } else {
            // Peek the operator stack and let topOp be top operator
            char topOp = operatorStack.peek();
            if (precedence(op) > precedence(topOp)) {
                operatorStack.push(op);
            } else {
                // Pop all stacked operators with equal or higher precedence than op
                while (!operatorStack.empty() && precedence(op) <= precedence(topOp)) {
                    operatorStack.pop();
                    postfix.append(topOp);
                    postfix.append(" ");
                    if (!operatorStack.empty()) {
                        // Reset topOp
                        topOp = operatorStack.peek();
                    }
                }
                
                // assert: Operator stack is empty or current operator precedence 
                //         > top of stack operator precedence
                operatorStack.push(op);
            }
        }
    }
    
    private boolean isOperator(char ch) {
        return OPERATORS.indexOf(ch) != -1;
    }
    
    private int precedence(char op) {
        return PRECEDENCE[OPERATORS.indexOf(op)];
    }
}
