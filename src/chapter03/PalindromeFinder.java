package chapter03;

import java.util.Stack;

public class PalindromeFinder {

    private String inputString;
    private Stack<Character> charStack = new Stack<Character>();

    public PalindromeFinder(String str) {
        inputString = str;
        fillStack();
    }

    private void fillStack() {
        if (inputString != null) {
            for (int i = 0; i < inputString.length(); i++) {
                charStack.push(inputString.charAt(i));
            }
        }
    }

    private String buildReverse() {
        StringBuilder result = new StringBuilder();
        while (!charStack.empty()) {
            // Remove top item from stack and append it to result.
            result.append(charStack.pop());
        }
        return result.toString();
    }

    public boolean isPalindrome() {
        if (inputString == null) {
            return false;
        }
        return inputString.equalsIgnoreCase(buildReverse());
    }

}
