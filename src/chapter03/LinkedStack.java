package chapter03;

import java.util.EmptyStackException;

public class LinkedStack<E> implements StackInt<E> {
    
    private Node<E> topOfStackRef = null;
    
    @Override
    public E push(E obj) {
        topOfStackRef = new Node<E>(obj, topOfStackRef);
        return obj;
    }
    
    @Override
    public E pop() {
        if (empty()) {
            throw new EmptyStackException();
        } else {
            E result = topOfStackRef.data;
            topOfStackRef = topOfStackRef.next;
            return result;
        }        
    }
    
    @Override
    public E peek() {
        if (empty()) {
            throw new EmptyStackException();
        } else {
            return topOfStackRef.data;
        }
    }
    
    @Override
    public boolean empty() {
        return topOfStackRef == null;
    }

    private static class Node<E> {
        // Data Fields
        private E data;
        private Node<E> next;
        
        private Node(E dataItem) {
            data = dataItem;
            next = null;
        }
        
        private Node(E dataItem, Node<E> nodeRef) {
            data = dataItem;
            next = nodeRef;
        }
    }
}
