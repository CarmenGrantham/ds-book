package chapter03;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeFinderTest {

	@Test
	public void isPalindrome_Glenelg_True() {
		PalindromeFinder pal = new PalindromeFinder("Glenelg");
		assertTrue(pal.isPalindrome());	
	}
	
	@Test
	public void isPalindrome_Null_False() {
		PalindromeFinder pal = new PalindromeFinder(null);
		assertFalse(pal.isPalindrome());	
	}
	
	@Test
	public void isPalindrome_a_True() {
		PalindromeFinder pal = new PalindromeFinder("a");
		assertTrue(pal.isPalindrome());	
	}
	
	@Test
	public void isPalindrome_blankString_True() {
		PalindromeFinder pal = new PalindromeFinder("");
		assertTrue(pal.isPalindrome());	
	}
	
	@Test
	public void isPalindrome_noon_True() {
		PalindromeFinder pal = new PalindromeFinder("noon");
		assertTrue(pal.isPalindrome());	
	}
}
