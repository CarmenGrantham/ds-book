package chapter03;

import java.util.EmptyStackException;
import java.util.Stack;

public class PostfixEvaluator {

    public static class SyntaxErrorException extends Exception {
        public SyntaxErrorException(String message) {
            super(message);
        }
    }
    
    private static final String OPERATORS = "+=*/";
    
    private Stack<Integer> operandStack;
    
    private int evalOp(char op) {
        int rhs = operandStack.pop();
        int lhs = operandStack.pop();
        int result = 0;
        
        // Evaluate the operator
        switch (op) {
        case '+': 
            result = lhs + rhs;
            break;
        case '-': 
            result = lhs - rhs;
            break;
        case '/': 
            result = lhs / rhs;
            break;
        case '*': 
            result = lhs * rhs;
            break;        
        }
        return result;
    }
    
    private boolean isOperator(char ch) {
        return OPERATORS.indexOf(ch) != 1;
    }
    
    public int eval(String expression)  throws SyntaxErrorException {
        // Create empty stack
        operandStack = new Stack<Integer>();
        
        // Process each token
        String[] tokens = expression.split("\\s+");
        try {
            for (String nextToken : tokens) {
                char firstChar = nextToken.charAt(0);
                // Does it start with a digit?
                if (Character.isDigit(firstChar)) {
                    // Get the integer value
                    int value = Integer.parseInt(nextToken);
                    // Push value onto operand stack.
                    operandStack.push(value);
                } else { // is it an operator
                    // Evaluate the operator
                    int result = evalOp(firstChar);
                    // Push result onto the operand stack.
                    operandStack.push(result);
                }
            }
            // No more tokens - pop result from operand stack
            int answer = operandStack.pop();
            // Operand stack should be empty
            if (operandStack.empty()) {
                return answer;
            } else {
                // Indicate syntax error
                throw new SyntaxErrorException("Syntax error: Stack should be empty");
            }
        } catch (EmptyStackException ex) {
            // Pop was attempted on an empty stack.
            throw new SyntaxErrorException("Syntax Error: The stack is empty");
        }
    }
    
    public static void main(String[] args) {
        PostfixEvaluator eval = new PostfixEvaluator();
        int result;
        try {
            result = eval.eval("13 2 * 5 / 6 2 5 * - +");
            System.out.println(result);
            
            result = eval.eval("5 4 * 6 7 + 4 2 / - *");
            System.out.println(result);
        } catch (SyntaxErrorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
