package chapter03;

import javax.swing.JOptionPane;

import chapter03.InfixToPostfix.SyntaxErrorException;

public class TestInfixPostfix {

    public static void main(String[] args) {
        InfixToPostfix inToPost = new InfixToPostfix();
        String infix = JOptionPane.showInputDialog("Enter an infix expression:");
        try {
            String postfix = inToPost.convert(infix);
            JOptionPane.showMessageDialog(null, "Infix expression " + infix + "\nconverts to " + postfix);
        } catch (SyntaxErrorException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
}
