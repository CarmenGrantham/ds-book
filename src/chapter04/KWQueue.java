package chapter04;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class KWQueue<E> implements Queue<E> {
    private LinkedList<E> theQueue = new LinkedList<E>();
    
    @Override
    public boolean offer(E item) {
        theQueue.addLast(item);
        return true;        
    }
    
    @Override
    public E poll() {
        if (size() == 0) {
            return null;
        } else {
            return theQueue.remove(0);
        }
    }
    
    @Override
    public E peek() {

        if (size() == 0) {
            return null;
        } else {
            return theQueue.getFirst();
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return theQueue.addAll(c);
    }

    @Override
    public void clear() {
        theQueue.clear();        
    }

    @Override
    public boolean contains(Object o) {
        return theQueue.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return theQueue.containsAll(c);
    }

    @Override
    public boolean isEmpty() {
        return theQueue.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return theQueue.iterator();
    }

    @Override
    public boolean remove(Object o) {
        return theQueue.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return theQueue.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return theQueue.retainAll(c);
    }

    @Override
    public int size() {
        return theQueue.size();
    }

    @Override
    public Object[] toArray() {
        return theQueue.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return theQueue.toArray(a);
    }

    @Override
    public boolean add(E arg0) {
        return theQueue.add(arg0);
    }

    @Override
    public E element() {
        return theQueue.element();
    }

    @Override
    public E remove() {
        return theQueue.remove();
    }

}
