package chapter04;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

public class ListQueue<E> extends AbstractQueue<E> implements Queue<E> {
    
    private Node<E> front;
    private Node<E> rear;
    private int size;
    

    private static class Node<E> {
        // Data Fields
        private E data;
        private Node<E> next = null;
        private Node<E> prev = null;
        
        private Node(E dataItem) {
            data = dataItem;
        }
    }

    @Override
    public boolean offer(E item) {
        if (front == null) {
            rear = new Node<E>(item);
            front = rear;
        } else {
            rear.next = new Node<E>(item);
            rear = rear.next;
        }
        size++;
        return true;
    }
    
    @Override
    public E poll() {
        E item = peek();        // Retrieve item at front
        if (item == null) {
            return null;
        }
        // Remove item at front
        front = front.next;
        size--;
        return item;            // Return data at front of queue
    }
    
    @Override
    public E peek() {
        if (size == 0) {
            return null;
        } else {
            return front.data;
        }
    }

    @Override
    public Iterator<E> iterator() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int size() {
        return size;
    }    
}
