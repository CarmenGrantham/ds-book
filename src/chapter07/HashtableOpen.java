package chapter07;

public class HashtableOpen<K, V> implements KWHashMap<K, V> {

    private Entry<K, V>[] table;
    private static final int START_CAPACITY = 101;
    private double LOAD_THRESHOLD = 0.75;
    private int numKeys;
    private int numDeletes;
    private final Entry<K, V> DELETED = new Entry<K,V>(null, null);
    
    
    public HashtableOpen() {
        table = new Entry[START_CAPACITY];
    }
    
    private int find(Object key) {
        // Calculate the starting index
        int index = key.hashCode() % table.length;
        if (index < 0) {
            index += table.length;      // Make it positive
        }
        
        // Increment table index until an empty slot is reached
        // or key is found
        while ((table[index] != null)
                && (!key.equals(table[index].key))) {
            index++;
            // Check for wraparound
            if (index >= table.length) {
                index = 0;      // wrap around
            }            
        }
        return index;
    }
    
    public V get(Object key) {
        // Find the first table element that is empty
        // or the table element that contains the key
        int index = find(key);
        
        // If the search is successful, return the value.
        if (table[index] != null) {
            return table[index].value;
        } else {
            return null;        // key not found
        }
    }
    
    public V put(K key, V value) {
        // Find the first table element that is empty
        // or the table element that contains the key
        int index = find(key);
        
        // If an empty element was found, insert new entry.
        if (table[index] == null) {
            table[index] = new Entry<K, V>(key, value);
            numKeys++;
            // Check whether rehash is needed
            double loadFactor = (double) (numKeys + numDeletes) / table.length;
            if (loadFactor > LOAD_THRESHOLD) {
                rehash();
            }
            return null;
        }
        
        // assert: table element that contains the key was found
        // Replace value for this key
        V oldVal = table[index].value;
        table[index].value = value;
        return oldVal;
    }

    private void rehash() {
        // Save reference to oldTable
        Entry<K,V>[] oldTable = table;
        // Double capacity of this table.
        table = new Entry[2 * oldTable.length + 1];
        
        // Reinsert all items in oldTable into expanded table.
        numKeys = 0;
        numDeletes = 0;
        for (int i = 0; i < oldTable.length; i++) {
            if ((oldTable[i] != null) && (oldTable[i] != DELETED)) {
                // Insert entry in expanded table
                put(oldTable[i].key, oldTable[i].value);
            }
        }
    }
 

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public V remove(Object key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

    private static class Entry<K, V> {
        private K key;
        private V value;
        
        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
        
        public K getKey() {
            return key;
        }
        
        public V getValue() {
            return value;
        }
        
        public V setValue(V val) {
            V oldVal = value;
            value = val;
            return oldVal;
        }
    }
    
}
