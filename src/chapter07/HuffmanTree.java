package chapter07;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import chapter06.BinaryTree;

public class HuffmanTree extends chapter06.HuffmanTree implements Serializable {

    private Map<Character, BitString> codeMap;
    
    public static HuffData[] buildFreqTable(BufferedReader ins) {
        // Map of frequencies
        Map<Character, Integer> frequencies =
                new HashMap<Character, Integer>();
        
        try {
            int nextChar;       // For storing the next character
            while ((nextChar = ins.read()) != -1) { // Test for more data
                // Get the current count and increment
                Integer count = frequencies.get((char) nextChar);
                if (count == null) {
                    count = 1;      // First occurrence
                } else {
                    count++;
                }
                
                //Store updated count.
                frequencies.put((char) nextChar, count);                
            }
            ins.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        
        // copy Map entries to a HuffData[] array
        HuffData[] freqTable = new HuffData[frequencies.size()];
        int i = 0;      // Start at beginning of array
        // Get each map entry and store it in the array
        // as a weight-symbol pair
        for (Map.Entry<Character, Integer> entry : frequencies.entrySet()) {
            freqTable[i] =
                    new HuffData(entry.getValue().doubleValue(), entry.getKey());
            i++;
                    
        }
        return freqTable;       // Return the array
    }
    
    public void buildCodeTable() {
        // Initialise the code map
        codeMap = new HashMap<Character, BitString>();
        // Call recursive method with empty bit string for code so far
        buildCodeTable(huffTree, new BitString());
    }
    
    private void buildCodeTable(BinaryTree<HuffData> tree, BitString code) {
        // Get data at local root
        HuffData datum = tree.getData();
        if (datum.symbol != null) {     // Test for leaf node
            // Found a symbol, insert its code in the map
            codeMap.put(datum.symbol, code);
        } else {
            // Append 0 to code so far and traverse left
            BitString leftCode = (BitString) code.clone();
            leftCode.append(false);         // false is {}
            buildCodeTable(tree.getLeftSubtree(), leftCode);
            
            // append 1 to code so far and traverse right
            BitString rightCode = (BitString) code.clone();
            rightCode.append(true);     // true is 1
            buildCodeTable(tree.getRightSubtree(), rightCode);
        }
    }
    
    public void encode(BufferedReader ins, ObjectOutputStream outs) {
        BitString result = new BitString();     // The complete bit string
        try {
            int nextChar;
            while ((nextChar = ins.read()) != -1) {     // More data?
                Character next = (char) nextChar;
                
                // Get bit stirng corresponding to symbol nextChar
                BitString nextChunk = codeMap.get(next);
                result.append(nextChunk);       // Append to result string                
            }
            
            // Write result to output file and close files
            result.trimCapacity();
            outs.writeObject(result);
            ins.close();
            outs.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
