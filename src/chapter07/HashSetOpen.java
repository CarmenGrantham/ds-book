package chapter07;

import java.util.AbstractSet;
import java.util.Iterator;

public class HashSetOpen<K> extends AbstractSet<K> {

    private KWHashMap<K, K> setMap = new HashtableOpen<K, K>();
    
    public boolean contains(Object key) {
        // HashtableOpen.get returns null if the key is not found
        return (setMap.get(key) != null);
    }
    
    public boolean add(K key) {
        // HashtableOpen.put returns null if the key is not a duplicate
        return (setMap.put(key, key) == null);
    }
    
    public boolean remove(Object key) {
        // HashtableOpen.remove returns null if the key is not removed
        return (setMap.remove(key) != null);
    }
    
    public int size() {
        return setMap.size();
    }

    @Override
    public Iterator<K> iterator() {
        // TODO Auto-generated method stub
        return null;
    }
}
