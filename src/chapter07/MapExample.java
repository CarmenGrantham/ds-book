package chapter07;

import java.util.HashMap;
import java.util.Map;

public class MapExample {

    public static void main(String[] args) {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("J", "Jane");
        aMap.put("B", "Bill");
        aMap.put("S", "Sam");
        aMap.put("B1", "Bob");
        aMap.put("B2", "Bill");
        
        System.out.println("B1 maps to " + aMap.get("B1"));
        System.out.println("Bill maps to " + aMap.get("Bill"));     // Returns null as Bill isn't a key
    }
}
