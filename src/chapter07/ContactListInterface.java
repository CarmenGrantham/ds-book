package chapter07;

import java.util.List;

public interface ContactListInterface {

    List<String> addOrChangeEntry(String name, List<String> numbers);
    List<String> lookupEntry(String name);
    List<String> removeEntry(String name);
    void display();
}
