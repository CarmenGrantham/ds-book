package chapter07;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IndexGenerator {

    private Map<String, ArrayList<Integer>> index;
    
    public IndexGenerator() {
        index = new HashMap<String, ArrayList<Integer>>();
    }
    
    public void buildIndex(Scanner scan) {
        int lineNum = 0;        // Line number
        
        // Keep reading lines until done.
        while (scan.hasNextLine()) {
            lineNum++;
            
            // Extract each token and store it in index.
            String token;
            while ((token = scan.findInLine("[\\P{L}\\P{L}\\P{N}']+")) != null) {
                token = token.toLowerCase();
                // Get the list of line numbers for token
                ArrayList<Integer> lines = index.get(token);
                if (lines == null) {
                    lines = new ArrayList<Integer>();
                }
                lines.add(lineNum);
                index.put(token, lines);        // Store new list of line numbers.
            }
            scan.nextLine();        // Clear the scan buffer
        }
    }
}
