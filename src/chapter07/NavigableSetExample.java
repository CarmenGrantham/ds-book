package chapter07;

import java.util.NavigableSet;
import java.util.TreeSet;

public class NavigableSetExample {

    public static void main(String[] args) {
        // Create and fill the sets
        NavigableSet<Integer> odds = new TreeSet<Integer>();
        odds.add(5);
        odds.add(3);
        odds.add(7);
        odds.add(9);
        odds.add(1);
        System.out.println("The original set odds is " + odds);
        NavigableSet b = odds.subSet(1,  false,  7, true);
        System.out.println("The ordered set b is " + b);
        System.out.println("Its first element is " + b.first());
        System.out.println("Its smallest element >= 6 is " + b.ceiling(6));
        
    }
}
