package chapter07;

import java.util.LinkedList;

public class HashtableChain<K,V> implements KWHashMap<K, V> {

    private LinkedList<Entry<K,V>>[] table;
    private int numKeys;
    private static final int CAPACITY = 101;
    private static final double LOAD_THRESHOLD = 3.0;
    
    public HashtableChain() {
        table = new LinkedList[CAPACITY];
    }
    
    public V get(Object key) {
        int index = key.hashCode() % table.length;
        if (index < 0) {
            index += table.length;
        }
        if (table[index] == null) {
            return null;        // key is not in table
        }
        
        // Search the list at table[index] to find key
        for(Entry<K, V> nextItem : table[index]) {
            if (nextItem.key.equals(key)) {
                return nextItem.value;
            }
        }
        
        // assert: key is not in the table
        return null;
    }
    
    public V put(K key, V value) {
        int index = key.hashCode() % table.length;
        if (index < 0) {
            index += table.length;
        }
        if (table[index] == null) {
            // Create a new linked list at table[index]
            table[index] = new LinkedList<Entry<K,V>>();
        }
        
        // Search the list at table[index] to find the key.
        for (Entry<K,V> nextItem : table[index]) {
            // If the search is successful, replace the old value.
            if (nextItem.key.equals(key)) {
                // REplace value for this key
                V oldVal = nextItem.value;
                nextItem.setValue(value);
                return oldVal;
            }
        }
        
        // assert: key is not in the table, add new item
        table[index].addFirst(new Entry<K,V>(key, value));
        numKeys++;
        if (numKeys > (LOAD_THRESHOLD * table.length)) {
            rehash();
        }
        return null;
    }
    
    private void rehash() {
        // TODO Implement method
    }
     
    
    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public V remove(Object key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

    private static class Entry<K, V> {
        private K key;
        private V value;
        
        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
        
        public K getKey() {
            return key;
        }
        
        public V getValue() {
            return value;
        }
        
        public V setValue(V val) {
            V oldVal = value;
            value = val;
            return oldVal;
        }
    }
}
