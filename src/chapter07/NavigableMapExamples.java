package chapter07;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;

public class NavigableMapExamples {

    public static double computeAverage(Map<Integer, Double> valueMap) {
        int count = 0;
        double sum = 0;
        for (Map.Entry<Integer, Double> entry : valueMap.entrySet()) {
            sum += entry.getValue().doubleValue();
            count++;
        }
        return (double) sum / count;
    }
    
    public static List<Double> computeSpans(NavigableMap valueMap, int delta) {
        List<Double> result = new ArrayList<Double>();
        Integer min = (Integer)valueMap.firstEntry().getKey();
        Integer max = (Integer)valueMap.lastEntry().getKey();
        
        for (int index = min; index <= max; index += delta) {
            double average =
                    computeAverage(valueMap.subMap(index, true, index=delta, false));
            result.add(average);
        }
        return result;
    }
}
