package chapter06;

import java.util.Scanner;
import java.util.TreeSet;

public class IndexGenerator {

    private TreeSet<String> index;
    
    public IndexGenerator() {
        index = new TreeSet<String>();
    }
    
    public void buildIndex(Scanner scan) {
        int lineNum = 0;        // line number
        
        // Keep reading lines until done
        while (scan.hasNextLine()) {
            lineNum++;
            
            // Extract each token and store it in index.
            String token;
            while ((token = scan.findInLine("[\\p{L}\\p{N}']+")) != null) {
                token = token.toLowerCase();
                index.add(String.format("%s, %3d", token, lineNum));
            }
            scan.nextLine();        // Clear the scan buffer
        }
    }
    
    public void showIndex() {
        // Use iterator to access and display tree data
        for (String next : index) {
            System.out.println(next);
        }
    }
}
