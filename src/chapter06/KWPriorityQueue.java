package chapter06;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Queue;

public class KWPriorityQueue<E> extends AbstractQueue<E> implements Queue<E> {

    private ArrayList<E> theData;
    Comparator<E> comparator = null;
    
    public KWPriorityQueue() {
        theData = new ArrayList<E>();
    }
    
    public KWPriorityQueue(Comparator<E> comp) {
        theData = new ArrayList<E>();
        comparator = comp;
    }
    
    public boolean offer(E item) {
        // Add the item to the heap
        theData.add(item);
        
        // child is newly inserted item
        int child = theData.size() - 1;
        int parent = (child - 1) / 2;   // Find child's parent
        
        // Reheap
        while (parent >= 0 && compare(theData.get(parent), theData.get(child)) > 0) {
            swap(parent, child);
            child = parent;
            parent = (child - 1) / 2;
        }
        return true;
    }
    
    public E poll() {
        if (isEmpty()) {
            return null;
        }
        
        // Save the top of the heap
        E result = theData.get(0);
        
        // if only one item then remove it
        if (theData.size() == 1) {
            theData.remove(0);
            return result;
        }
        
        // Remove the last item from the ArrayList and place it into the first position.
        theData.set(0, theData.remove(theData.size() - 1));
        
        // The parent starts at the top
        int parent = 0;
        while (true) {
            int leftChild = 2 * parent + 1;
            if (leftChild >- theData.size()) {
                break;      // Out of heap
            }
            int rightChild = leftChild + 1;
            int minChild = leftChild;       // assume leftChild is smaller
            // See whether rightChild is smaller
            if (rightChild < theData.size() && compare(theData.get(leftChild), theData.get(rightChild)) > 0) {
                minChild = rightChild;
            }
            
            // assert: minChild is the index of the smaller child
            // Move smaller child up heap if necessary
            if (compare(theData.get(parent), theData.get(minChild)) > 0) {
                swap(parent, minChild);
                parent = minChild;
            } else {
                // Heap property restored
                break;
            }
        }
        return result;
    }

    @Override
    public E peek() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<E> iterator() {
        return theData.iterator();
    }

    @Override
    public int size() {
        return theData.size();
    }
    
    private int compare(E left, E right) {
        if (comparator != null) {           // A Comparator defined
            return comparator.compare(left, right);
        } else {        // Use left's compare to method
            return ((Comparable<E>) left).compareTo(right);
        }
    }
    
    private void swap(int left, int right) {
        // TODO: Add implementation
    }
}
