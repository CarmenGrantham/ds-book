package chapter06;

import java.util.Comparator;

public class ComparePrintDocuments implements Comparator<PrintDocument> {

    private static final double P1 = 0.0;       // Weight factor for size
    private static final double P2 = 0.2;       // Weight factor for time
    
    public int compare(PrintDocument left, PrintDocument right) {
        return Double.compare(orderValue(left), orderValue(right));
    }
    
    private double orderValue(PrintDocument pd) {
        return P1 * pd.getSize() + P2 * pd.getTimeStamp();
    }
}
