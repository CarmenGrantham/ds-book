package chapter06;

import java.util.Random;

public class SkipList<E extends Comparable<E>> implements SearchTree<E> {
    
    // Natural log of 2
    static final double LOG2 = Math.log(2.0);

    // Static class to contain data and the links
    static class SLNode<E> {
        SLNode<E>[] links;
        E data;
        
        // Create a node of level m
        SLNode (int m, E data) {
            links = (SLNode<E>[]) new SLNode[m];    // create links
            this.data = data;                       // store items
        }
    }
    
    int maxLevel = 2;
    // Nominal maximum capacity
    int maxCap = computeMaxCap(maxLevel);
    // A random number generator
    final static Random rand = new Random();
    // The current size of the skipList
    int size;
    
    SLNode<E> head = new SLNode<E>(maxLevel, null);
    
    /**
     * Method to compute the maximum capacity, given the maximum
     * level. It computes Math.pow(2, maxLevel) - 1, using shift.
     * @param maxLevel
     * @return Math.pow(2, maxLevel+1 - 1
     */
    private static int computeMaxCap(int maxLevel) {
        return ~(~0 << maxLevel);
    }
    
    
    /**
     * Search for an item in the list
     * @param target The item being sought.
     * @return An SLNode array which references the predecessors
     *         of the target at each level
     */
    private SLNode<E>[] search (E target) {
        SLNode<E>[] pred = (SLNode<E>[]) new SLNode[maxLevel];
        SLNode<E> current = head;
        for (int i = current.links.length-1; i >= 0; i--) {
            while (current.links[i] != null
                    && current.links[i].data.compareTo(target) < 0) {
                current = current.links[i];
            }
            pred[i] = current;
        }
        return pred;
    }
    
    /**
     * Find an object in the skip list
     * @param target The item being sought
     * @return A reference to the object in the skip-list that matches
     *         the target. If not found, null is returned.
     */
    public E find(E target) {
        SLNode<E>[] pred = search(target);
        if (pred[0].links[0] != null
                && pred[0].links[0].data.compareTo(target) == 0) {
            return pred[0].links[0].data;
        } else {
            return null;
        }
    }
    
    /**
     * Method to generate a logarithmic distributed integer between
     * 1 and maxLevel. i.e., 1/2 of the values returns are 1, 1/4
     * are 2, 1/8 are 3, etc.
     * @return A random logarithmic distributed int between 1 an maxLevel
     */
    private int logRandom() {
        int r = rand.nextInt(maxCap);
        int k = (int) (Math.log(r + 1) / LOG2);
        if (k > maxLevel - 1) {
            k = maxLevel - 1;
        }
        return maxLevel - k;
    }
    
    public boolean add(E target) {
        // TODO implement add method
        return false;
    }
    
    /**
     * Determine if an item is in the tree.
     * 
     * @param target The item being sought
     * @return true if the item is in the tree, false otherwise
     */
    @Override
    public boolean contains(E target) {
        return find(target) != null;
    }
    
    /**
     * Removes target from tree
     * @post target is not in the tree
     * @param target Item to be removed
     * @return true if the object was in the tree, false otherwise
     */
    @Override
    public boolean remove(E target) {
        return delete(target) != null;
    }
    
    /**
     * Remove all data from tree
     */
    public void clear() {
        for (int i = 0; i < maxLevel; i++) {
            head.links[i] = null;
        }
        size = 0;
    }


    @Override
    public E delete(E target) {
        // TODO Auto-generated method stub
        return null;
    }
}
