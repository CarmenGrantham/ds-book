package chapter05;

public class Maze implements GridColors {
    
    private TwoDimGrid maze;

    public Maze(TwoDimGrid m) {
        maze = m;
    }
    
    public boolean findMazePath() {
        return findMazePath(0, 0);      // 0,0 is the start point
    }
    
    public boolean findMazePath(int x, int y) {
        if (x < 0 || y < 0
                || x >= maze.getNCols() || y >= maze.getNRows()) {
            return false;           // Cell is out of bounds
        } else if (!maze.getColor(x, y).equals(BACKGROUND)) {
            return false;           // Cell is on barrier or dead end
        } else if (x == maze.getNCols() - 1 && y == maze.getNRows() - 1) {
            maze.recolor(x, y, PATH);           // Cell is on path
            return true;                        // and is maze exit;
        } else {            // Recursive case
            // Attempt to find path from each neighbour
            // Tentatively mark cell as on path
            maze.recolor(x,  y, PATH);
            if (findMazePath(x - 1, y) || findMazePath(x + 1, y) || findMazePath(x, y - 1) || findMazePath(x, y + 1)) {
                return true;
            } else {
                maze.recolor(x,  y, TEMPORARY);                     // Dead end
                return false;
            }
        }
    }
}
