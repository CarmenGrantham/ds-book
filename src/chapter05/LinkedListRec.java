package chapter05;

public class LinkedListRec<E> {
    
    private Node<E> head;

    private int size(Node<E> head) {
        if (head == null) {
            return 0;
        }
        return 1 + size(head.next);
    }
    
    public int size() {
        return size(head);
    }
    
    private String toString(Node<E> head) {
        if (head == null) {
            return "";
        }
        return head.data + "\n" + toString(head.next);
    }
    
    public String toString() {
        return toString(head);
    }
    
    private void replace(Node<E> head, E oldObj, E newObj) {
        if (head != null) {
            if (oldObj.equals(head.data)) {
                head.data = newObj;
            }
            replace(head.next, oldObj, newObj);
        }
    }
    
    public void replace(E oldObj, E newObj) {
        replace(head, oldObj, newObj);
    }
    
    private void add(Node<E> head, E data) {
        // If list has just 1 element add to it
        if (head.next == null) {
            head.next = new Node<E>(data);
        } else {
            add(head.next, data);       // Add to rest of list
        }
    }
    
    public void add(E data) {
        if (head == null) {
            head = new Node<E>(data);       // List has 1 node
        } else {
            add(head, data);
        }
    }
    
    private boolean remove(Node<E> head, Node<E> pred, E outData) {
        if (head == null) {     // Base case - empty list
            return false;
        } else if (head.data.equals(outData)) {      // 2nd base case
            pred.next = head.next;      // Remove head
            return true;
        } else {
            return remove(head.next, head, outData);
        }
    }
    
    public boolean remove(E outData) {
        if (head == null) {
            return false;
        } else if (head.data.equals(outData)) {
            head = head.next;
            return true;
        } else {
            return remove(head.next, head, outData);
        }
    }
    
    private static class Node<E> {
        // Data Fields
        private E data;
        private Node<E> next = null;
        private Node<E> prev = null;
        
        private Node(E dataItem) {
            data = dataItem;
        }
    }

}
