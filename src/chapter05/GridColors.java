package chapter05;

import java.awt.Color;

public interface GridColors {
    
    public static final Color BACKGROUND = Color.RED;
    public static final Color NON_BACKGROUND = Color.GRAY;
    public static final Color ABNORMAL = Color.WHITE;
    public static final Color TEMPORARY = Color.PINK;
    
    public static final Color PATH = Color.YELLOW;
}