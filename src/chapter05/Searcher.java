package chapter05;

public class Searcher {

    private static int linearSearch(Object[] items, Object target, int posFirst) {
        if (posFirst == items.length) {
            return -1;
        } else if (target.equals(items[posFirst])) {
            return posFirst;
        }
        return linearSearch(items, target, posFirst + 1);
    }
    
    private static int linearSearch(Object[] items, Object target) {
        return linearSearch(items, target, 0);
    }
    
    private static int binarySearch(Object[] items, Comparable target, int first, int last) {
        System.out.println("binary search: first = " + first + ", last = " + last);
        if (first > last) {
            return -1;      // base case for unsuccessful search
        } else {
            int middle = (first + last) / 2;    // next probe index
            int compResult = target.compareTo(items[middle]);
            if (compResult == 0) {
                return middle;      // Base case for successful search
            } else if (compResult < 0) {
                return binarySearch(items, target, first, middle - 1);
            }
            return binarySearch(items, target, middle + 1, last);
        }
    }
    
    private static int binarySearch(Object[] items, Comparable target) {
        return binarySearch(items, target, 0, items.length - 1);
    }
    
    public static void main(String[] args) {
        String[] greetings = {"Hi", "Hello", "Shalom"};
        int posHello = linearSearch(greetings, "Hello");
        int posGoodbye = linearSearch(greetings, "Goodbye");
        System.out.println("Hello position: " + posHello + ", Goodbye position: " + posGoodbye);
        
        
        String[] kidNames = {"Andrew", "Bert", "Carlos", "Deanna", "Eric", "Frank", "George", "Kelly", "Laura"};
        int posEric = binarySearch(kidNames, "Eric");
        System.out.println("Eric position is " + posEric);
        int posAndrew = binarySearch(kidNames, "Andrew");
        System.out.println("Andrew position is " + posAndrew);
        
        
    }
}
