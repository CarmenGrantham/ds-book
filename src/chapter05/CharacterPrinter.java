package chapter05;

public class CharacterPrinter {

    public static void printChars(String str) {
        if (str == null || str.equals("")) {
            return;
        }
        System.out.println(str.charAt(0));
        printChars(str.substring(1));
    }
    
    public static void printCharsReverse(String str) {
        if (str == null || str.equals("")) {
            return;
        }
        printCharsReverse(str.substring(1));
        System.out.println(str.charAt(0));
    }
    
    public static void main(String[] args) {
        printChars("Hello");
        System.out.println();
        printCharsReverse("World");
    }
}
