package chapter05;

public class CalculateLength {

    public static int length(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }
        return 1 + length(str.substring(1));
    }
    
    public static void main(String[] args) {
        int catLength = length("cat");
        System.out.println("Length of cat is " + catLength);
        
        int hippoLength = length("hippopotamous");
        System.out.println("Length of hippo is " + hippoLength);
    }
}
