package chapter05;

public class TowersOfHanoi {

    public static String showMoves(int n, char startPeg, char destPeg, char tempPeg) {
        if (n == 1) {
            return "Move disk 1 from peg " + startPeg
                    + " to peg " + destPeg + "\n";
        } else {        // Recursive step
            return showMoves(n - 1, startPeg, tempPeg, destPeg)
                    + "Move disk " + n + " from peg "  + startPeg
                    + " to peg " + destPeg + "\n"
                    + showMoves(n - 1, tempPeg, destPeg, startPeg);
        }
    }
    
    public static void main(String[] args) {
        int nDisks = 3;
        char startPeg = 'L';
        char tempPeg = 'M';
        char destPeg = 'R';
        
        String moves = showMoves(nDisks, startPeg, destPeg, tempPeg);
        System.out.println(moves);
    }
}
