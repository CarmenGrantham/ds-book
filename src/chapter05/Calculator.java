package chapter05;

import javax.print.attribute.standard.Finishings;

public class Calculator {

    public static int factorial(int n) {
        if (n == 0) {
            return 1;
        }
        return n * factorial (n - 1);
    }
    
    public static int factorialIter(int n) {
        int result = 1;
        for (int k = 1; k <= n; k++) {
            result = result * k;
        }
        return result;
    }
    
    public static double power(double x, int n) {
        if (n == 0) {
            return 1;
        }
        return x * power(x, n - 1);
    }
    
    // Calculate the Greatest common divisor m and n
    public static double gcd(int m, int n) {
        if (m % n == 0) {
            return n;
        } else if (m < n) {
            return gcd(n, m);   // Swap arguments.
        }
        return gcd(n, m % n);
    }
    
    public static int fibonacci(int n) {
        if (n <= 2) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
    
    public static int fibonnaciImprovedStart(int n) {
        return fibonacciImproved(1,  0, n);
    }
    
    // Improved version of fibonnaci
    public static int fibonacciImproved(int fibCurrent, int fibPrevious, int n) {
        if (n == 1) {
            return fibCurrent;
        }
        return fibonacciImproved(fibCurrent + fibPrevious, fibCurrent, n - 1);
    }
    
    public static void main(String[] args) {
        System.out.println("Factorial 4 is " + factorial(4));
        
        System.out.println("Power of 2 to 4 is " + power(2, 4));
        
        System.out.println("gcd(30, 20) is " + gcd(30, 20));
        
        System.out.println("fibonacci of 4 is " + fibonacci(4));
        
        System.out.println("fibonacci improved of 4 is " + fibonnaciImprovedStart(4));
    }
}
