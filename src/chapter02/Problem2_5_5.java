package chapter02;

public class Problem2_5_5 {

	public static void main(String[] args) {
		executeA();
		executeB();
		executeC();
		executeD();
	}
	
	private static void executeA() {
		System.out.println("Problem a....");
		SingleLinkedList names = new SingleLinkedList();
		names.addFirst("Sam");
        names.addFirst("Harry");
        names.addFirst("Dick");
        names.addFirst("Tom");
        System.out.println("PRE: " + names);
        names.executeProblem2_5_5a("Shakira");
        System.out.println("POST: " + names);
	}
	
	
	private static void executeB() {
		System.out.println("\nProblem b....");
		SingleLinkedList names = new SingleLinkedList();
		names.addFirst("Sam");
        names.addFirst("Harry");
        names.addFirst("Dick");
        names.addFirst("Tom");
        System.out.println("PRE: " + names);
        names.executeProblem2_5_5b();
        System.out.println("POST: " + names);		
	}
	
	private static void executeC() {
		System.out.println("\nProblem c....");
		SingleLinkedList names = new SingleLinkedList();
		names.addFirst("Sam");
        names.addFirst("Harry");
        names.addFirst("Dick");
        names.addFirst("Tom");
        System.out.println("PRE: " + names);
        names.executeProblem2_5_5c("Tamika");
        System.out.println("POST: " + names);		
	}
	
	private static void executeD() {
		System.out.println("\nProblem d....");
		SingleLinkedList names = new SingleLinkedList();
		names.addFirst("Sam");
        names.addFirst("Harry");
        names.addFirst("Dick");
        names.addFirst("Tom");
        System.out.println("PRE: " + names);
        names.executeProblem2_5_5d("Harry", "Sally");
        System.out.println("POST: " + names);		
	}
}
