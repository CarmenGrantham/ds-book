package chapter02;

import java.util.ArrayList;
import java.util.List;

public class Example2_2 {

    public static void main(String[] args) {
        ArrayList yourList = new ArrayList();
        yourList.add(new Integer(35));
        yourList.add("bunny");
        yourList.add(new Double(3.14));
        
        printList(yourList);
        
        String animal = (String) yourList.get(1);
        System.out.println("Animal is " + animal);
        
    }
    
    private static void printList(List list) {
        System.out.println("Printing list....");
        for (Object item : list) {
            System.out.println(item);
        }
        System.out.println();
    }
}
