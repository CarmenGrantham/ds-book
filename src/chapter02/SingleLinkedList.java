package chapter02;

public class SingleLinkedList<E> {

    private Node<E> head = null;
    private int size;
    
    public SingleLinkedList() {
    
    }
        
    public void populateWithTestData() {
    	Node<String> tom = new Node<String>("Tom");
    	Node<String> dick = new Node<String>("Dick");
    	Node<String> harry = new Node<String>("Harry");
    	Node<String> sam = new Node<String>("Sam");
    	tom.next = dick;
    	dick.next = harry;
    	harry.next = sam;
    	
    }
    
    public void addFirst(E item) {
        head = new Node<E>(item, head);
        size++;
    }
    
    private void addAfter(Node<E> node, E item) {
        node.next = new Node<E>(item, node.next);
        size++;
    }
    
    private E removeAfter(Node<E> node) {
        Node<E> temp = node.next;
        if (temp != null) {
            node.next = temp.next;
            size--;
            return temp.data;
        } else {
            return null;
        }
    }
    
    private E removeFirst() {
        Node<E> temp = head;
        if (head != null) {
            head = head.next;
        }
        // Return data at old head or null if list is empty
        if (temp != null) {
            size--;
            return temp.data;
        } else {
            return null;
        }
    }
    
    private Node<E> getNode(int index) {
        Node<E> node = head;
        for (int i = 0; i < index && node != null; i++) {
            node = node.next;
        }
        return node;
    }
    
    public E get(int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        Node<E> node = getNode(index);
        return node.data;
    }
    
    public E set(int index, E newValue) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        Node<E> node = getNode(index);
        E result = node.data;
        node.data = newValue;
        return result;
    }
    
    public void add(int index, E item) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        if (index == 0) {
            addFirst(item);
        } else {
            Node<E> node = getNode(index-1);
            addAfter(node, item);
        }
    }
    
    public boolean add(E item) {
        add(size, item);
        return true;
    }
    
    
    
    public String toString() {
        Node<E> nodeRef = head;
        StringBuilder result = new StringBuilder();
        while (nodeRef != null) {
            result.append(nodeRef.data);
            if (nodeRef.next != null) {
                result.append(" ==> ");
            }
            nodeRef = nodeRef.next; // Advance down the list
        }
        return result.toString();
    }
    
    
    public void executeProblem2_5_5a(E data) {
    	head = new Node<E>(data, head.next);
    }
    
    public void executeProblem2_5_5b() {
    	Node<E> nodeRef = head.next;
    	nodeRef.next = nodeRef.next.next;
    }
    
    public void executeProblem2_5_5c(E data) {
    	Node<E> nodeRef = head;
		while (nodeRef.next != null)
		     nodeRef = nodeRef.next;
		nodeRef.next = new Node<E>(data);
    }

    public void executeProblem2_5_5d(E search, E replace) {
    	Node<E> nodeRef = head;
    	while (nodeRef != null && !nodeRef.data.equals(search))
    	     nodeRef = nodeRef.next;
    	if (nodeRef != null) {
    	     nodeRef.data = replace;
    	     nodeRef.next = new Node<E>(search, nodeRef.next.next);
    	}
    }
    
    
    
    private static class Node<E> {
        // Data Fields
        private E data;
        private Node<E> next;
        
        private Node(E dataItem) {
            data = dataItem;
            next = null;
        }
        
        private Node(E dataItem, Node<E> nodeRef) {
            data = dataItem;
            next = nodeRef;
        }
    }
}
