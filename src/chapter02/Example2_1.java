package chapter02;

import java.util.ArrayList;
import java.util.List;

public class Example2_1 {
	public static void main(String[] args) {
        List<String> myList = new ArrayList<String>();
        myList.add("Bashful");
        myList.add("Awful");
        myList.add("Jumpy");
        myList.add("Happy");
        
        printList(myList);
        
        myList.add(2, "Doc");
        printList(myList);
        
        myList.add("Dopey");		// Added to end of list
        printList(myList);
        
        myList.remove(1);			// Remove Awful
        printList(myList);
        
        String dwarf = myList.get(2);
        System.out.println("Dwarf at position 2 is " + dwarf);
        
        myList.set(2, "Sneezy");	// overwrite item at position 2
        printList(myList);
        
        int jumpyIndex = myList.indexOf("Jumpy");
        System.out.println("Jumpy index is " + jumpyIndex);
        
        int sneezyIndex = myList.indexOf("Sneezy");
        System.out.println("Sneezy index is " + sneezyIndex);

	}
	
	private static void printList(List<String> list) {
        System.out.println("Printing list....");
        for (String item : list) {
            System.out.println(item);
        }
        System.out.println();
	}

}
