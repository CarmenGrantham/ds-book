package chapter02;

public class Problem2_3_1 {

	public static void main(String[] args) {
		// Part 1
		System.out.println("Part 1.......");
		int[] anArray = {0, 1, 2, 3, 4, 5, 6, 7};
		printArray(anArray);
		for (int i = 3; i < anArray.length - 1; i++) {
		     anArray[i+1] = anArray[i];
		}
		printArray(anArray);
		
		// Part 2
		System.out.println("\nPart 2.......");
		int[] anArray2 = {0, 1, 2, 3, 4, 5, 6, 7};
		printArray(anArray2);
		for (int i = anArray2.length - 1; i > 3; i--) {
			anArray2[i] = anArray2[i - 1];
		}
		printArray(anArray2);
	}

	private static void printArray(int[] array) {
		for (int item : array) {
			System.out.print(item + " ");
		}
		System.out.println();
	}
}
