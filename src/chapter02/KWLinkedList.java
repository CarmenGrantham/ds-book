package chapter02;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class KWLinkedList<E> {
    // Data Fields
    /* Reference head of the list */
    private Node<E> head = null;
    /* Reference the end of the list */
    private Node<E> tail = null;
    /* The size of the list */
    private int size = 0;
    
    public KWLinkedList() {
        
    }
    
    public KWLinkedList(Node<E> head, Node<E> tail, int size) {
        this.head = head;
        this.tail = tail;
        this.size = size;
        if (size > 0) {
            tail.next = null;
            head.prev = null;
        }
    }
    
    public ListIterator<E> listIterator() {
        return new KWListIter(0);
    }
    
    public ListIterator<E> listIterator(int index) {
        return new KWListIter(index);
    }
    
    public void addFirst(E item) {
        add(0, item);
    }
    
    public void addLast(E item) {
        add (size, item);
    }
    
    public void add(int index, E obj) {
        listIterator(index).add(obj);
    }
    
    public E getFirst() {
        return head.data;
    }
    
    public E getLast() {
        return tail.data;
    }
    
    public E get(int index) {
        return listIterator(index).next();
    }
    
    
    private class KWListIter implements ListIterator<E> {
        private Node<E> nextItem;
        private Node<E> lastItemReturned;
        private int index = 0;
        
        public KWListIter(int i) {
            // Validate i parameter
            if (i < 0 || i > size) {
                throw new IndexOutOfBoundsException("Invalid index " + i);
            }
            lastItemReturned = null;
            // Special case of last item
            if (i == size) {
                index = size;
                nextItem = null;
            } else {        // Start at the beginning
                nextItem = head;
                for (index = 0; index < i; index++) {
                    nextItem = nextItem.next;
                }
            }            
        }
        
        public boolean hasNext() {
            return nextItem != null;
        }
        
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastItemReturned = nextItem;
            nextItem = nextItem.next;
            index++;
            return lastItemReturned.data;
        }
        
        public boolean hasPrevious() {
            return (nextItem == null && size != 0)
                    || nextItem.prev != null;
        }
        
        public E previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            if (nextItem == null) {         // Iterator is past the last element
                nextItem = tail;
            } else {
                nextItem = nextItem.prev;
            }
            lastItemReturned = nextItem;
            index--;
            return lastItemReturned.data;
        }
        
        public void add(E obj) {
            if (head == null) {         // Add to an empty list
                head = new Node<E>(obj);
                tail = head;
            } else if (nextItem == head) {          // Insert at head
                // Create new node
                Node<E> newNode = new Node<E>(obj);
                // Link it to the nextItem
                newNode.next = nextItem;
                // Link nextItem to the new node
                nextItem.prev = newNode;
                // The new node is now the head
                head = newNode;
            } else if (nextItem == null) {          // Insert at tail
                // Create new node
                Node<E> newNode = new Node<E>(obj);
                // Link the tail to the new node
                tail.next = newNode;
                // Link the new node to the tail
                newNode.prev = tail;
                // The new node is the new tail
                tail = newNode;
            } else {
                // Create new node
                Node<E> newNode = new Node<E>(obj);
                // Link it to nextItem.prev
                newNode.prev = nextItem.prev;
                nextItem.prev.next = newNode;
                // Link it the nextItem
                newNode.next = nextItem;
                nextItem.prev = newNode;
                
            }
            
            // Increase size and index and set lastItemReturned
            size++;
            index++;
            lastItemReturned = null;
        }
        
        public void remove() {
            if (lastItemReturned == null) {
                throw new IllegalStateException();
            }
            // unlink this item from its next neighbour
            if (lastItemReturned.next != null) {
                lastItemReturned.next.prev = lastItemReturned.prev;
            } else {                    // item is the tail
                tail = lastItemReturned.prev;
                if (tail != null) {
                    tail.next = null;
                } else {                    // list is now empty
                    head = null;
                }
            }
            
            // Unlink item form prev neighbour
            if (lastItemReturned.prev != null) {
                lastItemReturned.prev.next = lastItemReturned.next;
            } else {                        // Item is the head
                head = lastItemReturned.next;
                if (head != null) {
                    head.prev = null;
                } else {
                    tail = null;
                }
            }
            
            // Invalidated lastItemReturned
            lastItemReturned = null;
            // Decrement size and index
            size--;
            index--;
        }
        
        public boolean remove(E target) {
            Node<E> current = head;
            while (current != null && !target.equals(current.data)) {
                current = current.next;
            }
            
            if (current == null) {
                return false;
            }
            
            // Unlink item from next neighbour
            if (current.next != null) {
                current.next.prev = current.prev;
            } else {                // Item is the tail
                tail = current.prev;
                if (tail != null) {
                    tail.next = null;
                } else {            // List is now empty
                    head = null;
                }
            }
            
            // Unlink item from prev neighbour
            if (current.prev != null) {
                current.prev.next = current.next;
            } else {                // Item is the head
                head = current.next;
                if (head != null) {
                    head.prev = null;
                } else {
                    tail = null;;
                }
            }
            // Decrement size and index
            size--;
            index--;
            return true;
        }

        @Override
        public int nextIndex() {
            return index;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void set(E item) {
            if (lastItemReturned == null) {
                throw new IllegalStateException();
            }
            lastItemReturned.data = item;            
        }
    }
    
    private static class Node<E> {
        // Data Fields
        private E data;
        private Node<E> next = null;
        private Node<E> prev = null;
        
        private Node(E dataItem) {
            data = dataItem;
        }
    }
    
    private void addItemsExample() {
        Node<String> tom = new Node<String>("Tom");
        Node<String> dick = new Node<String>("Dick");
        Node<String> harry = new Node<String>("Harry");
        Node<String> sam = new Node<String>("Sam");
        
        tom.next = dick;
        
        dick.next = harry;
        dick.prev = tom;
        
        harry.next = sam;
        harry.prev = dick;
        
        sam.prev = harry;
        
        
        Node<String> sharon = new Node<String>("Sharon");
        sharon.next = sam;
        sharon.prev = sam.prev;
        sam.prev.next = sharon;
        sam.prev = sharon;
        
        
        // Removing items
        harry.prev.next = harry.next;
        harry.next.prev = harry.prev;
        
    }
}
