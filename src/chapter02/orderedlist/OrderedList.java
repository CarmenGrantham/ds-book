package chapter02.orderedlist;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class OrderedList<E extends Comparable<E>> implements Iterable<E> {

    private LinkedList<E> theList = new LinkedList<E>();
    
    public void add(E obj) {
        ListIterator<E> iter = theList.listIterator();
        while (iter.hasNext()) {
            if (obj.compareTo(iter.next()) < 0) {
                // Iterator has stepped over the first element
                // that is greater than the element to be inserted
                // Move the iterator back one
                iter.previous();
                // Insert the element
                iter.add(obj);
                return;
            }                
        }
        // Add new item to end of list
        iter.add(obj);
    }
    
    public E get(int index) {
        return theList.get(index);
    }
    
    public Iterator<E> iterator() {
        return theList.iterator();
    }
    
    public int size() {
        int size = 0;
        Iterator<E> iter = iterator();
        while(iter.hasNext()) {
            iter.next();
            size++;
        }
        return size;
    }
    
    public E remove(E obj) {
        ListIterator<E> iter = theList.listIterator();
        while(iter.hasNext()) {
            if (obj == iter.next()) {
                iter.remove();
                return obj;
            }
        }
        return null;
    }
}
