package chapter02;

public class Problem2_4_1 {

	public static void main(String[] args) {
		int n = 4;
		
		// Part a
		System.out.println("Part a....");
		for (int i = 0; i < n; i++) 
		     for (int j = 0; j < n; j++)
		          System.out.println(i + " " + j);
		
		// Part b
		System.out.println("\nPart b....");
		for (int i = 0; i < n; i++) 
		     for (int j = 0; j < 2; j++)
		          System.out.println(i + " " + j);

		// Part c
		System.out.println("\nPart c....");
		for (int i = 0; i < n; i++) 
		     for (int j = n - 1; j >= i; j--)
		          System.out.println(i + " " + j);

		// Part d
		System.out.println("\nPart d....");
		for (int i = 1; i < n; i++) 
		     for (int j = 0; j < i; j++)
		          if (j % i == 0)
		               System.out.println(i + " " + j);

		System.out.println("----");
		System.out.println(2%2);
		System.out.println(0%2);

	}
	
}
