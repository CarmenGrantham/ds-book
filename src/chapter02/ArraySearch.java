package chapter02;

/**
 * Provides static method search for searching array
 * 
 * @author carmen
 *
 */
public class ArraySearch {

    public static int search(int[] x, int target) {
        for (int i = 0; i < x.length; i++) {
            if (x[i] == target) {
                return i;
            }
        }
        
        // target not found
        return -1;
    }
    
    public static void main(String[] args) {
        int[] x = {5, 12, 15, 4, 8, 12, 7}; // Array to search;
        
        // Test for target at first element
        verify(x, 5, 0);
        // Test for target as last element
        verify(x, 7, 6);
        // Test for target not in array
        verify(x, -5, -1);
        // Test for multiple occurrences of target
        verify(x, 12, 1);
        // Test for target somewhere in middle
        verify(x, 4, 3);
        
        // Test for 1-element array
        x = new int[1];
        x[0] = 10;
        verify(x, 10, 0);
        verify(x, -10, -1);
        
        // Test for an empty array
        x = new int[0];
        verify(x, 10, -1);
    }
    
    private static void verify(int[] x, int target, int expected) {
        int actual = search(x, target);
        System.out.print("Search(x, " + target + ") is "
                + actual + ", expected " + expected);
        if (actual == expected) {
            System.out.println(": Pass");
        } else {
            System.out.println(": ****Fail");
        }
    }
}
