package chapter02;

public class SingleLinkedListTest {

    public static void main(String[] args) {
        SingleLinkedList<String> names = new SingleLinkedList<String>();
        names.addFirst("Sam");
        names.addFirst("Harry");
        names.addFirst("Dick");
        names.addFirst("Tom");
        
        System.out.println(names);
    }
    
}
