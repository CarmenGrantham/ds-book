package chapter08;

public class ShellSort {

    public static <T extends Comparable<T>> void sort(T[] table) {
        // Gap between adjacent cells
        int gap = table.length / 2;
        while (gap > 0) {
            for (int nextPos = gap; nextPos < table.length; nextPos++) {
                // Insert element at nextPos in its subarray
                insert(table, nextPos, gap);
            }
            
            // Reset gap for next pass
            if (gap == 2) {
                gap = 1;
            } else {
                gap = (int) (gap / 2.2);
            }
        }
    }
    
    private static <T extends Comparable<T>> void insert(T[] table, int nextPos, int gap) {
        T nextVal = table[nextPos]; // Element to insert
        // Shift all values > nextVal to subarray down by gap
        while ((nextPos > gap -1)
                && (nextVal.compareTo(table[nextPos - gap]) < 0)) {
            table[nextPos] = table[nextPos - gap];      // shift down
            nextPos -= gap;     // Check next position in subarray
        }
        table[nextPos] = nextVal;   // Insert nextVal
    }
}
