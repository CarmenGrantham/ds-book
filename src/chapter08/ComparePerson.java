package chapter08;

import java.util.Comparator;

public class ComparePerson implements Comparator<Person> {

    @Override
    public int compare(Person left, Person right) {
        return left.getBirthDay() - right.getBirthDay();
    }
}
