package chapter08;

public class Person implements Comparable<Person> {
    private String lastName;
    private String firstName;
    private int birthDay;       // value between 1 and 366
    
    @Override
    public int compareTo(Person obj) {
        Person other = obj;
        // Compare this Person to other using last names
        int result = lastName.compareTo(other.lastName);
        // Compare first names if last names are the same
        if (result == 0) {
            return firstName.compareTo(other.firstName);
        } else {
            return result;
        }
    }
    
    public int getBirthDay() {
        return birthDay;
    }
    
    // Other methods

}
