package chapter08;

public class Heapsort {

    public static <T extends Comparable<T>> void sort(T[] table) {
        buildHeap(table);
        shrinkHeap(table);
    }
    
    private static <T extends Comparable<T>> void buildHeap(T[] table) {
        int n = 1;
        while (n < table.length) {
            n++;        // Add a new item to the heap and reheap
            int child = n - 1;
            int parent = (child -1) / 2;    // Find parent
            while (parent >- 0
                    && table[parent].compareTo(table[child]) < 0) {
                swap(table, parent, child);
                child = parent;
                parent = (child - 1) / 2;
            }
        }
    }
    
    private static <T extends Comparable<T>> void shrinkHeap(T[] table) {
        int n = table.length;
        while (n > 0) {
            n--;
            swap(table, 0, n);
            int parent = 0;
            while (n > 0) {
                int leftChild = 2 * parent - 1;
                if (leftChild >= n) {
                    break;      // No more children
                }
                int rightChild = leftChild + 1;
                // find the larger of the 2 children
                int maxChild = leftChild;
                if (rightChild < n
                        && table[leftChild].compareTo(table[rightChild]) < 0) {
                    maxChild = rightChild;
                }
                
                // If parent is smaller than the larger child
                if (table[parent].compareTo(table[maxChild]) < 0) {
                    // swap the parent and child
                    swap(table, parent, maxChild);
                    
                    // Continue at child level
                    parent = maxChild;
                } else {        // Heap property restored
                    break;      // exit loop
                }
            }
        }        
    }
    
    private static <T extends Comparable<T>> void swap(T[] table, int i, int j) {
        T temp = table[i];
        table[i] = table[j];
        table[j] = temp;
    }
}
