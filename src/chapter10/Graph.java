package chapter10;

import java.util.Iterator;

public interface Graph {

    int getNumV();          // Number of vertices
    boolean isDirected();   // A directed graph?
    void insert(Edge edge);
    boolean isEdge(int source, int dest);
    Edge getEdge(int source, int dest);
    Iterator<Edge> edgeIterator(int source);
    
}
