package chapter10;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

public class DepthFirstSearch {

    private Graph graph;
    private int[] parent;
    private boolean[] visited;          // Flag indicating visit status
    private int[] discoveryOrder;       // Has each vertex in discovery order
    private int[] finishOrder;          // Has each vertex in finish order
    private int discoverIndex;
    private int finishIndex = 0;
    
    public DepthFirstSearch(Graph graph) {
        this.graph = graph;
        int n = graph.getNumV();
        parent = new int[n];
        visited = new boolean[n];
        discoveryOrder = new int[n];
        finishOrder = new int[n];
        for (int i = 0; i < n; i++) {
            parent[i] = -1;
        }
        for (int i = 0; i < n; i++) {
            if (!visited[i]) {
                depthFirstSearch(i);
            }
        }
    }
    
    public DepthFirstSearch(Graph graph, int[] order) {
        this.graph = graph;
        int n = graph.getNumV();
        parent = new int[n];
        visited = new boolean[n];
        discoveryOrder = new int[n];
        finishOrder = new int[n];
        for (int i = 0; i < n; i++) {
            parent[i] = -1;
        }
        for (int i = 0; i < n; i++) {
            if (!visited[order[i]]) {
                depthFirstSearch(order[i]);
            }
        }
    }
    
    public void depthFirstSearch(int current) {
        // mark current vertex visited
        visited[current] = true;
        discoveryOrder[discoverIndex] = current;
        
        // Examine each vertex adjacent to the current vertex
        Iterator<Edge> itr = graph.edgeIterator(current);
        while (itr.hasNext()) {
            int neighbour = itr.next().getDest();
            // Process neighbour that has not been visited
            if (!visited[neighbour]) {
                // Insert (current, neighbour) into depth-first seach tree
                parent[neighbour] = current;
                // Recursively apply the algorithm starting at neighbour
                depthFirstSearch(neighbour);
            }
        }
        
        // Mark current finished
        finishOrder[finishIndex++] = current;
        
        
    }
    
    public int[] getDiscoveryOrder() {
        return discoveryOrder;
    }
    
    public int[] getFinishOrder() {
        return finishOrder;
    }
    
    public static void main(String[] args) {
        Graph g = null;
        int n = 0;
        try {
            Scanner scan = new Scanner(new File(args[0]));
            g = AbstractGraph.createGraph(scan, true, "List");
            n = g.getNumV();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(1);         // Error
        }
        
        // Perform depth-first search
        DepthFirstSearch dfs = new DepthFirstSearch(g);
        int[] dOrder = dfs.getDiscoveryOrder();
        int[] fOrder = dfs.getFinishOrder();
        System.out.println("Discovery and finish order");
        for (int i = 0; i < n; i++) {
            System.out.println(dOrder[i] + " " + fOrder[i]);
        }
    }
}
