package chapter10;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

public class Maze {

    public static void main(String[] args) {
        int numV = 0;       // Number of vertices
        Graph theMaze = null;
        
        // Load the graph data from a file
        try {
            Scanner scan = new Scanner(new File(args[0]));
            theMaze = AbstractGraph.createGraph(scan, false, "List");
            numV = theMaze.getNumV();
        } catch (IOException ex) {
            System.err.println("IO Error while reading graph");
            System.err.println(ex.toString());
            System.exit(1);
        }
        
        // Perform breadth-first search
        int parent[] = BreadthFirstSearch.breadthFirstSearch(theMaze, 0);
        
        // Construct the path
        Stack thePath = new Stack();
        int v = numV - 1;
        while (parent[v] != -1) {
            thePath.push(new Integer(v));
            v = parent[v];
        }
        
        // Output the path
        System.out.println("The shortest path is:");
        while (!thePath.empty()) {
            System.out.println(thePath.pop());
        }
    }
}
