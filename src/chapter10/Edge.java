package chapter10;

public class Edge {

    private int dest;       // destination vertex
    private int source;     // source vertex
    private double weight;
    
    public Edge(int source, int dest) {
        this.source = source;
        this.dest = dest;
    }
    
    public Edge(int source, int dest, double weight) {
        this.source = source;
        this.dest = dest;
        this.weight = weight;
    }
    
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof Edge) {
            Edge e = (Edge)o;
            return e.dest == this.dest && e.source == this.source;
        }
        return false;
    }
    
    public int getDest() {
        return dest;
    }
    
    public int getSource() {
        return source;
    }
    
    public double getWeight() {
        return weight;
    }
    
    public int hashCode() {
        return (31 * source) + (37 * dest);
    }
    
    public String toString() {
        return "Source: " + source
                + " Destination: " + dest
                + " Weight: " + weight;
    }
}
