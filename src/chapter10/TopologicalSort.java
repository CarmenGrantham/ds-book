package chapter10;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class TopologicalSort {
    public static void main(String[] args) {
        int numVertices = 0;       // Number of vertices
        Graph theGraph = null;
        
        // Load the graph data from a file
        try {
            Scanner scan = new Scanner(new File(args[0]));
            theGraph = AbstractGraph.createGraph(scan, true, "List");
            numVertices = theGraph.getNumV();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        
        // Perform depth-first search
        DepthFirstSearch dfs = new DepthFirstSearch(theGraph);
        // Obtain finish order
        int[] finishOrder = dfs.getFinishOrder();
        
        // Print vertices in reverse finish order
        System.out.println("The Topological Sort is");
        for (int i = numVertices - 1; i >= 0; i--) {
            System.out.println(finishOrder[i]);
        }
    }
}
