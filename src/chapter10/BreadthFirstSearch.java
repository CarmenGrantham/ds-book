package chapter10;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstSearch {

    public static int[] breadthFirstSearch(Graph graph, int start) {
        Queue<Integer> theQueue = new LinkedList<Integer>();
        // Declare array parent and initialise its elements to -1
        int[] parent = new int[graph.getNumV()];
        for (int i = 0; i < graph.getNumV(); i++) {
            parent[i] = -1;
        }
        
        // Declare array identified and
        // initialise its elements to false
        boolean[] identified = new boolean[graph.getNumV()];
        // marke start vertex as identified and insert it into queue
        identified[start] = true;
        theQueue.offer(start);
        
        // Perform breadth first search until done
        while (!theQueue.isEmpty()) {
            // take vertex, current, out of the queue (begin visiting current)
            int current = theQueue.remove();
            
            // Examine each vertex, neighbour, adjacent to current
            Iterator<Edge> itr = graph.edgeIterator(current);
            while (itr.hasNext()) {
                Edge edge = itr.next();
                int neighbour = edge.getDest();
                // If neighbour has not been identified
                if(!identified[neighbour]) {
                    // Mark it identified
                    identified[neighbour] = true;
                    // Place it into the queue
                    theQueue.offer(neighbour);
                    // Insert edge (current, neighbour) into tree
                    parent[neighbour] = current;
                }
            }
            // Finished visiting current
        }
        return parent;
    }
}
